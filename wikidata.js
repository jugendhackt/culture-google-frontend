class SPARQLQueryDispatcher {
	constructor( endpoint ) {
		this.endpoint = endpoint;
	}
	query( sparqlQuery ) {
		const fullUrl = this.endpoint + '?query=' + encodeURIComponent( sparqlQuery );
		const headers = { 'Accept': 'application/sparql-results+json' };

		return fetch( fullUrl, { headers } ).then( body => body.json() );
	}
}

const endpointUrl = 'https://query.wikidata.org/sparql';
const sparqlQuery = `#Liste der aktuell exisiterenden Staaten und Hauptstädten
SELECT DISTINCT ?country ?countryLabel ?capitalLabel ?size ?population ?flagimage ?highestpointLabel ?meter ?lowestpointLabel ?meters ?continentLabel ?officialheldbyheadofstateLabel ?officiallanguageLabel
WHERE
{
  ?country wdt:P31 wd:Q3624078 .
  #not a former country
  FILTER NOT EXISTS {?country wdt:P31 wd:Q3024240}
  #and no an ancient civilisation (needed to exclude ancient Egypt)
  FILTER NOT EXISTS {?country wdt:P31 wd:Q28171280}
  OPTIONAL { ?country wdt:P36 ?capital } .
 OPTIONAL { ?country wdt:P2046 ?size } .
OPTIONAL { ?country wdt:P1082 ?population } .
OPTIONAL { ?country wdt:P41 ?flagimage} .
OPTIONAL { 
  ?country wdt:P610 ?highestpoint .
?highestpoint wdt:P2044 ?meter .
} .
OPTIONAL { 
  ?country wdt:P1589 ?lowestpoint .
 ?lowestpoint wdt:P2044 ?meters .
} .
OPTIONAL { ?country wdt:P30 ?continent} .
OPTIONAL { ?country wdt:P1906 ?officialheldbyheadofstate} .
OPTIONAL { ?country wdt:P361 ?partof} .
OPTIONAL { ?country wdt:P37 ?officiallanguage} .

  SERVICE wikibase:label { bd:serviceParam wikibase:language "de" }
}
ORDER BY ?countryLabel`;

const queryDispatcher = new SPARQLQueryDispatcher( endpointUrl );
 
var laender = ["Spanien", "Thailand", "Kroatien", "Japan", "Italien", "Vereinigtes Königreich", "Malaysia", "Tschechien", "Frankreich", "Deutschland", "Tunesien", "Vereinigte Arabische Emirate", "Vereinigte Staaten"]


async function exportData(land) {

    let data = await queryDispatcher.query( sparqlQuery );


    for (const country of data.results.bindings) {
        // console.log(country.countryLabel.value)
        if (country.countryLabel.value === land) {
            let landObj = {
                Kontinent: country.continentLabel.value,
                Hauptstadt: country.capitalLabel.value,
                Amtssprache: country.officiallanguageLabel.value,
                Regierungsoberhaupt: country.officialheldbyheadofstateLabel.value,
                Fläche: country.size.value,
                Einwohner: country.population.value,
                "Höchster Punkt": `${country.highestpointLabel.value} ${country.meter.value}Meter`
            }

            return landObj;
        }
    }
}