var map = L.map('map').setView([51.505, -0.09], 13);

L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
}).addTo(map);


async function onMapClick(e) {
    const { lat, lng } = e.latlng;
    
    const country = await coordinatesToCountry({
        lat: lat, lng: lng
    })
    
                

    const res = await fetch(`${ipAddress}/${country}`)

    if (res.status === 404) {
        alert(`Über ${country} gibt es momentan keine Änderungen.`)
    }

    const resJSON = await res.json();

    const wikidataData = await exportData(country)
    

    generateDetailView(country, {...resJSON,...wikidataData });

    document.getElementById('map').style.display = 'none';
    document.getElementById('detailPage').style.display = 'initial'
}


map.on('click', onMapClick)