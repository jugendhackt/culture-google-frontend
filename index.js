// const ipAddress = "http://192.168.178.42:5000"
const ipAddress = "http://127.0.0.1:5000"


function generateCountryListItem({ name, description }) {
    const listItem = document.createElement('li');
    const h3Element = document.createElement('h3');
    const paragraphElement = document.createElement('p');

    h3Element.innerText = name;
    paragraphElement.innerText = description;
    

    listItem.appendChild(h3Element);
    listItem.appendChild(paragraphElement)

    return listItem;
}



async function buildList() {
    const list =  document.getElementById('countryList');
    
    const response = await fetch(`${ipAddress}/infos`);

    const responseJSON = await response.json();

    for (var country of Object.keys(responseJSON)) {
        let des = responseJSON[country].Essgewonheiten
        list.appendChild(generateCountryListItem({ name: country, description: des}));
    }
}

function toggleMainView() {
    let map = document.getElementById('map');
    let detailPage = document.getElementById('detailPage');

    if (map.style.display === '' | detailPage.style.display === 'none') {
        map.style.display = 'none';
        detailPage.style.display = 'initial'
    } else if (map.style.display === 'none') {
        map.style.display = 'initial';
        detailPage.style.display = 'none'
    }
}

function generateDetailView(country, data) {
    document.getElementById('countryHeading').innerText = country

    let tbody = document.getElementById('tableBody');
    tbody.innerHTML = ''

    for (const [key, value] of Object.entries(data)) {
        let tableRow = document.createElement('tr')
        let category = document.createElement('td')
        let valueField = document.createElement('td')

        category.innerText = key;
        valueField.innerText = value;
        tableRow.appendChild(category);
        tableRow.appendChild(valueField);

        tbody.appendChild(tableRow);
    }
}

buildList()
    .then(() => {
        for (const listItem of document.getElementById('countryList').children) {
            listItem.addEventListener('click', async (event) => {
                const country = event.currentTarget.querySelector('h3').innerText;
                

                const res = await fetch(`${ipAddress}/${country}`)
                const resJSON = await res.json();

                const wikidataData = await exportData(country)

                generateDetailView(country, {...resJSON,...wikidataData });

                document.getElementById('map').style.display = 'none';
                document.getElementById('detailPage').style.display = 'initial'
            });
        }
    });
