async function coordinatesToCountry({lat, lng}) {
    const url = `https://nominatim.openstreetmap.org/reverse?lat=${lat}&lon=${lng}&format=json`;

    const jsonResult = fetch(url)
        .then((response) => {
            if (!response.ok) {
                throw new Error(`HTTP error: ${response.status}`)
            }
            return response;
        })
        .then((response) => {
            return response.json().then((jsonResponseRaw) => {
                return jsonResponseRaw.address.country;
            });
        })
        .then((countryName) => {
            return countryName;
        })
        .catch(() => {
            return undefined;
        })

        return jsonResult
    }